/*******************************************************************
 * MIT License
 *
 * Copyright (c) 2021-2022 Lukas Voreck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************/

import getDictionary from './dictionary-provider';

// Provides all objects
export default {
    get: function (identifier) {
        return getDictionary().then(function (dictionary) {
            // Provides the rootdir object
            if (identifier.key === 'rootdir') {
                return {
                    identifier: identifier,
                    name: dictionary.name,
                    type: 'folder',
                    location: 'ROOT'
                };

            } else if (identifier.key === "v."
                || identifier.key === "tar."
                || identifier.key === "o."
                || identifier.key === "s."
                || identifier.key === "n."
                || identifier.key === "dock."
                || identifier.key === "r.") {

                // Getting the first level subdir from the rootdir
                const firstSubdir = dictionary.measurements.filter(function (m) {
                    return m.key === identifier.key;
                })[0];

                // Provides all first level subdir objects
                return {
                    identifier: identifier,
                    name: firstSubdir.name,
                    type: "folder",
                    location: "vck.zll:rootdir"
                };

            } else if (identifier.key === "tar.o.") {

                // Getting the first level subdir from the rootdir
                const firstSubdir = dictionary.measurements.filter(function (m) {
                    return m.key === identifier.key.split(".")[0] + ".";
                })[0];
                // Getting the second level subdir from the rootdir
                const secondSubdir = firstSubdir.measurements.filter(function (m) {
                    return m.key === identifier.key;
                })[0];

                // Provides all second level subdir objects
                if (secondSubdir !== undefined) {
                    return {
                        identifier: identifier,
                        name: secondSubdir.name,
                        type: "folder",
                        location: "vck.zll:rootdir." + firstSubdir.key.slice(0, -1)
                    };
                }

            } else if (identifier.key.startsWith("tar.")
                || identifier.key.startsWith("v.")
                || identifier.key.startsWith("o.")
                || identifier.key.startsWith("s.")
                || identifier.key.startsWith("n.")
                || identifier.key.startsWith("dock.")
                || identifier.key.startsWith("r.")) {

                // Getting the first level subdir from the rootdir
                const firstSubdir = dictionary.measurements.filter(function (m) {
                    return m.key === identifier.key.split(".")[0] + ".";
                })[0];
                // Getting the telemetry object from the first level subdir
                const measurement = firstSubdir.measurements.filter(function (m) {
                    return m.key === identifier.key;
                })[0];

                // Provides all telemetry objects from first level subdirs
                if (measurement !== undefined) {
                    return {
                        identifier: identifier,
                        name: measurement.name,
                        type: 'kerbal.telemetry',
                        telemetry: {
                            values: measurement.values
                        },
                        location: 'vck.zll:rootdir.' + firstSubdir.key.slice(0, -1)
                    };
                }

            } else {

                // Getting the telemetry object from the root dir
                const measurement = dictionary.measurements.filter(function (m) {
                    return m.key === identifier.key;
                })[0];

                // Provides all telemetry objects from rootdir
                if (measurement !== undefined) {
                    return {
                        identifier: identifier,
                        name: measurement.name,
                        type: 'kerbal.telemetry',
                        telemetry: {
                            values: measurement.values
                        },
                        location: 'vck.zll:rootdir'
                    };
                }
            }
        });
    }
};
