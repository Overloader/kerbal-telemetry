/*******************************************************************
 * MIT License
 *
 * Copyright (c) 2021-2022 Lukas Voreck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************/

// The directories are split into multiple jsons for better overview. For the plugin to work they need to be in one so
// this file merges them

// Get all the dictionaries
const root = require("./dictionaries/root.json");
const v = require("./dictionaries/v.json");
const tar = require("./dictionaries/tar.json");
const tarO = require("./dictionaries/tar.o.json");
const o = require("./dictionaries/o.json");
const s = require("./dictionaries/s.json");
const n = require("./dictionaries/n.json");
const dock = require("./dictionaries/dock.json");
const r = require("./dictionaries/r.json");

// convert them to strings
const rootString = JSON.stringify(root);
const vString = JSON.stringify(v);
const tarString = JSON.stringify(tar);
const tarOString = JSON.stringify(tarO);
const oString = JSON.stringify(o);
const sString = JSON.stringify(s);
const nString = JSON.stringify(n);
const dockString = JSON.stringify(dock);
const rString = JSON.stringify(r);

// replace the placeholders in the root directory with the corresponding directories and convert back to JSON
const result = JSON.parse(
    rootString.replace("\"v\"", vString)
        .replace("\"tar\"", tarString)
        .replace("\"tar.o\"", tarOString)
        .replace("\"o\"", oString)
        .replace("\"s\"", sString)
        .replace("\"n\"", nString)
        .replace("\"dock\"", dockString)
        .replace("\"r\"", rString)
);

export default function () {
    return Promise.resolve(result);
}
