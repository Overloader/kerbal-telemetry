/*******************************************************************
 * MIT License
 *
 * Copyright (c) 2021-2022 Lukas Voreck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************/

import getDictionary from './dictionary-provider';

// Provides composites from the root dictionary
export const rootProvider = {
    appliesTo: function (domainObject) {
        return domainObject.identifier.namespace === 'vck.zll' && domainObject.identifier.key === 'rootdir';
    },

    load: function () {
        return getDictionary().then(function (dictionary) {
            // provides the composites
            return dictionary.measurements.map(function (m) {
                return {
                    namespace: 'vck.zll',
                    key: m.key
                };
            });
        });
    }
};

// Provides composites from all first level subdirectories
export const firstSubdirProvider = {
    appliesTo: function (domainObject) {
        return domainObject.identifier.namespace === 'vck.zll'
            && (domainObject.identifier.key === 'v.'
                || domainObject.identifier.key === "tar."
                || domainObject.identifier.key === "o."
                || domainObject.identifier.key === "s."
                || domainObject.identifier.key === "n."
                || domainObject.identifier.key === "dock."
                || domainObject.identifier.key === "r.");
    },

    load: function (domainObject) {
        return getDictionary().then(function (dictionary) {
            // gets subdirectory from root directory
            const firstSubdir = dictionary.measurements.filter(function (m) {
                return m.key === domainObject.identifier.key;
            })[0];

            // Provides the composites
            return firstSubdir.measurements.map(function (m) {
                return {
                    namespace: "vck.zll",
                    key: m.key
                };
            });
        });
    }
};

// Provides composites from all second level subdirectories
export const secondSubdirProvider = {
    appliesTo: function (domainObject) {
        return domainObject.identifier.namespace === 'vck.zll'
            && (domainObject.identifier.key === 'tar.o.');
    },

    load: function (domainObject) {
        return getDictionary().then(function (dictionary) {
            // gets the first level subdirectory from the root directory
            const firstSubdir = dictionary.measurements.filter(function (m) {
                return m.key === domainObject.identifier.key.split(".")[0] + ".";
            })[0];
            // gets the second level subdirectory from the first level subdirectory
            const secondSubdir = firstSubdir.measurements.filter(function (m) {
                return m.key === domainObject.identifier.key;
            })[0];

            // Provides the composites
            return secondSubdir.measurements.map(function (m) {
                return {
                    namespace: "vck.zll",
                    key: m.key
                };
            });
        });
    }
};
