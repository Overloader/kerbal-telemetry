$VALUES_JSON="      `"values`": [
    {
      `"key`": `"value`",
      `"name`": `"Value`",
      `"format`": `"float`",
      `"hints`": {
        `"range`": 1
      }
    },
    {
      `"key`": `"utc`",
      `"source`": `"timestamp`",
      `"name`": `"Timestamp`",
      `"format`": `"utc`",
      `"hints`": {
        `"domain`": 1
      }
    }
  ]
"

$NAME=$args[0]
$KEY=$NAME.replace(' ','')

$RESOURCE_JSON="    {
    `"name`": `"$NAME`",
    `"key`": `"r.resource.$KEY`",
$VALUES_JSON    },
"
$RESOURCE_CURRENT_JSON="    {
    `"name`": `"$NAME in current Stage`",
    `"key`": `"r.resourceCurrent.$KEY`",
$VALUES_JSON    },
"
$RESOURCE_CURRENT_MAX_JSON="    {
    `"name`": `"Max $NAME in current Stage`",
    `"key`": `"r.resourceCurrentMax.$KEY`",
$VALUES_JSON    },
"
$RESOURCE_MAX_JSON="    {
    `"name`": `"Max $NAME`",
    `"key`": `"r.resourceMax.$KEY`",
$VALUES_JSON    }
"

[Collections.Generic.List[String]]$R_JSON=Get-Content "$PSScriptRoot\src\plugins\kerbal-telemetry\dictionaries\r.json"

$R_JSON.RemoveAt($R_JSON.Count-1)
$R_JSON.RemoveAt($R_JSON.Count-1)
$R_JSON.RemoveAt($R_JSON.Count-1)
$R_JSON.Add("    },")
$R_JSON.Add("$RESOURCE_JSON")
$R_JSON.Add("$RESOURCE_CURRENT_JSON")
$R_JSON.Add("$RESOURCE_CURRENT_MAX_JSON")
$R_JSON.Add("$RESOURCE_MAX_JSON")
$R_JSON.Add("   ]")
$R_JSON.Add("}")

Out-File -FilePath "$PSScriptRoot\src\plugins\kerbal-telemetry\dictionaries\r.json" -InputObject $R_JSON.ToArray()